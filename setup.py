from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='prooooooooooooooooooject_2022-02-17_v1_dev',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    version=version,
    description='prooooooooooooooooooject',
    author='po_1 po_1'
)
